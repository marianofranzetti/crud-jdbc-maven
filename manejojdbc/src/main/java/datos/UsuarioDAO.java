/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package datos;

import static datos.Conexion.*;
import domain.*;
import java.sql.*;
import java.util.*;


public class UsuarioDAO {
    

    private static final String SQL_SELECT = "SELECT * FROM mariano.usuario";
    private static final String SQL_INSERT = "INSERT INTO mariano.usuario(usuario, password) VALUES (?,?)";
    private static final String SQL_UPDATE = "UPDATE mariano.usuario SET usuario= ?, password= ? WHERE IDUSUARIO= ?";
    private static final String SQL_DELETE = "DELETE FROM mariano.usuario WHERE IDUSUARIO = ?";

    
    public List<Usuario> obtenerTodos() throws SQLException{
        
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Usuario usuario = null;
        List<Usuario> usuarios = new ArrayList<>();
        
        conn = getConnection();
        stmt = conn.prepareStatement(SQL_SELECT);
        rs = stmt.executeQuery();
        while (rs.next()){
            int idUsuario = rs.getInt("IDUSUARIO");
            String usuarioo = rs.getString("usuario");
            String password = rs.getString("password");
           
            usuario = new Usuario(idUsuario, usuarioo, password);

            usuarios.add(usuario);
            
        }
            close(rs);
            close(stmt);
            close(conn);
            return usuarios;
    }


    public int insertar(Usuario usuario) throws SQLException{
        Connection conn = null;
        PreparedStatement stmt = null;
        int registros  = 0;
        
        conn = getConnection();
        stmt= conn.prepareStatement(SQL_INSERT);
        stmt.setString(1, usuario.getUsuario());
        stmt.setString(2, usuario.getPassword());
       
        registros = stmt.executeUpdate();
        
        
        close(stmt);
        close(conn);
        
        return registros;
    }
    
    public void actualizar(Usuario usuario) throws SQLException{
            
            List<Usuario> usuariosObtenidos = obtenerTodos();
            
            for (int i = 0; i < usuariosObtenidos.size(); i++) {
                if(usuariosObtenidos.get(i).getId() == usuario.getId()){
                    Usuario usuarioAgregar = new Usuario(usuario.getId(), usuario.getUsuario(), usuario.getPassword());
                    actualizarUsuario(usuarioAgregar);
                }
            }
        }

    public int actualizarUsuario(Usuario usuario) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        int registros  = 0;
 
        conn = getConnection();
        stmt= conn.prepareStatement(SQL_UPDATE);
        stmt.setString(1, usuario.getUsuario());
        stmt.setString(2, usuario.getPassword());
        stmt.setInt(3, usuario.getId());

        registros = stmt.executeUpdate();
        
        
        close(stmt);
        close(conn);
        
        return registros;
        
    }
    
    
    public void eliminarUsuario(int id) throws SQLException{
        
          List<Usuario> usuariosObtenidos = obtenerTodos();
            for (int i = 0; i < usuariosObtenidos.size(); i++) {
                if(usuariosObtenidos.get(i).getId()== id){
                    eliminarUsuarioDb(id);
                }
            }
    }

    private int eliminarUsuarioDb(int id) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        int registros  = 0;
 
        conn = getConnection();
        stmt= conn.prepareStatement(SQL_DELETE);
        stmt.setInt(1, id);
        
        registros = stmt.executeUpdate();
          
        close(stmt);
        close(conn);
        
        return registros;
    }

}

    
