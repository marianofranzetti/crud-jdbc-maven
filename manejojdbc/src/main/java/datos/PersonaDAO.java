/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package datos;

import static datos.Conexion.*;
import domain.Persona;
import java.sql.*;
import java.util.*;


public class PersonaDAO {
    
    private static final String SQL_SELECT = "SELECT idpersona, nombre, apellido, email, telefono, documento FROM mariano.persona";
    private static final String SQL_INSERT = "INSERT INTO mariano.persona(nombre, apellido, email, telefono, documento) VALUES (?,?,?,?,?)";
    private static final String SQL_UPDATE = "UPDATE mariano.persona SET nombre= ?, apellido= ?, email= ?, telefono= ?, documento= ? WHERE idpersona = ? ";
    private static final String SQL_DELETE = "DELETE FROM mariano.persona WHERE idpersona = ?";

    
    public List<Persona> seleccionar() throws SQLException{
        
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Persona persona = null;
        List<Persona> personas = new ArrayList<>();
        
        conn = getConnection();
        stmt = conn.prepareStatement(SQL_SELECT);
        rs = stmt.executeQuery();
        while (rs.next()){
            int idpersona = rs.getInt("idpersona");
            String nombre = rs.getString("nombre");
            String apellido = rs.getString("apellido");
            String documento = rs.getString("documento");
            String email = rs.getString("email");
            String telefono = rs.getString("telefono");
            
            persona = new Persona(idpersona, nombre, apellido, documento, email, telefono);

            personas.add(persona);
            
        }
            close(rs);
            close(stmt);
            close(conn);
            return personas;
    }


    public int insertar(Persona persona) throws SQLException{
        Connection conn = null;
        PreparedStatement stmt = null;
        int registros  = 0;
        
        conn = getConnection();
        stmt= conn.prepareStatement(SQL_INSERT);
        stmt.setString(1, persona.getNombre());
        stmt.setString(2, persona.getApellido());
        stmt.setString(3, persona.getEmail());
        stmt.setString(4, persona.getTelefono());
        stmt.setString(5, persona.getDocumento());
        
        registros = stmt.executeUpdate();
        
        
        close(stmt);
        close(conn);
        
        return registros;
    }
    
    public void actualizar(Persona persona) throws SQLException{
            
            List<Persona> personasObtenidas = seleccionar();
            
            for (int i = 0; i < personasObtenidas.size(); i++) {
                if(personasObtenidas.get(i).getIdPersona() == persona.getIdPersona()){
                    Persona personaActualizar = new Persona(persona.getIdPersona(), persona.getNombre(), persona.getApellido(), persona.getDocumento(), persona.getEmail(), persona.getTelefono());
                    actualizarPersona(personaActualizar);
                }
            }
        }

    public int actualizarPersona(Persona persona) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        int registros  = 0;
 
        conn = getConnection();
        stmt= conn.prepareStatement(SQL_UPDATE);
        stmt.setString(1, persona.getNombre());
        stmt.setString(2, persona.getApellido());
        stmt.setString(3, persona.getEmail());
        stmt.setString(4, persona.getTelefono());
        stmt.setString(5, persona.getDocumento());
        stmt.setInt(6, persona.getIdPersona());

        
         
        registros = stmt.executeUpdate();
        
        
        close(stmt);
        close(conn);
        
        return registros;
        
    }
    
    
    public void eliminarPersona(int id) throws SQLException{
        
          List<Persona> personasObtenidas = seleccionar();
            for (int i = 0; i < personasObtenidas.size(); i++) {
                if(personasObtenidas.get(i).getIdPersona() == id){
                    eliminarPersonaDb(id);
                }
            }
    }

    private int eliminarPersonaDb(int id) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        int registros  = 0;
 
        conn = getConnection();
        stmt= conn.prepareStatement(SQL_DELETE);
        stmt.setInt(1, id);
        
        registros = stmt.executeUpdate();
          
        close(stmt);
        close(conn);
        
        return registros;
    }

}
