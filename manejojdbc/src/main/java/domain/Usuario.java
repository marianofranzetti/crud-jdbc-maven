/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package domain;

/**
 *
 * @author Admin
 */
public class Usuario {
    
    int IDUSUARIO;
    String usuario;
    String password;

    public Usuario() {
    }

    public Usuario(int id, String usuario, String password) {
        this.IDUSUARIO = id;
        this.usuario = usuario;
        this.password = password;
    }

    public Usuario(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
    }

    public Usuario(int id) {
        this.IDUSUARIO = id;
    }

    public int getId() {
        return IDUSUARIO;
    }

    public void setId(int id) {
        this.IDUSUARIO = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + IDUSUARIO + ", usuario=" + usuario + ", password=" + password + '}';
    }
    
    
    
}
