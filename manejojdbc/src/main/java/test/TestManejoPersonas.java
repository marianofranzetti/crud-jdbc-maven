/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import datos.PersonaDAO;
import domain.Persona;
import java.sql.SQLException;
import java.util.List;

public class TestManejoPersonas {
    
    
    public static void main(String[] args) throws SQLException {
       //Persona personaNueva = new Persona(4,"juliio", "balmaceda", null, "julioo@milico.com", "1125487566");
        PersonaDAO persona = new PersonaDAO();
        //persona.insertar(personaNueva);
        //persona.actualizar(personaNueva);
        persona.eliminarPersona(4);
        List<Persona> personas = persona.seleccionar();

        for(Persona personaObtenida: personas){
           System.out.println(personaObtenida.getApellido());
        }
    }
    
}
