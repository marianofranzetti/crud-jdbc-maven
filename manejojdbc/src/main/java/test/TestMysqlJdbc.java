/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class TestMysqlJdbc {
    
    public static void main(String[] args) {
        
        String url = "jdbc:mysql://localhost:3306/mariano?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrievel=true";
        try {
            //Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conexion = DriverManager.getConnection(url, "root", "1234");
            Statement sentencia = conexion.createStatement();
            String sql = "SELECT * FROM mariano.persona";
            ResultSet resultado = sentencia.executeQuery(sql);
            while(resultado.next()){
                System.out.println(resultado.getInt("idpersona"));
                System.out.println(resultado.getString("nombre"));
            }
            resultado.close();
            sentencia.close();
            conexion.close();
        } catch (SQLException e) {
            Logger.getLogger(TestMysqlJdbc.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
